-- :name create-users-table :!
-- :doc Create users table
CREATE TYPE GENDER AS ENUM ('male', 'female');
CREATE TABLE Users (
	Usersid SERIAL NOT NULL,
	LastName varchar(40) NOT NULL,
	FirstName varchar(40) NOT NULL,
	Birth DATE NOT NULL,
	Gender GENDER,
	CreatedAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (Usersid)
)

-- :name create-blacklist-table :!
-- :doc creating the blacklist table
CREATE TABLE Blacklist (
	Recid INT NOT NULL,
	Tarid INT NOT NULL,
	PRIMARY KEY (Recid, Tarid)
)

-- :name create-request-table :!
-- :doc creating the request table
CREATE TABLE Request (
	Recid INT NOT NULL,
	Tarid INT NOT NULL,
	PRIMARY KEY (Recid, Tarid)
)

-- :name create-whitelist-table :!
-- :doc creating the request table
CREATE TABLE Whitelist (
	Recid INT NOT NULL,
	Tarid INT NOT NULL,
	PRIMARY KEY (Recid, Tarid)
)

-- :name all-users :? :*
-- :doc Get all users
SELECT * FROM Users ORDER BY Usersid

-- :name get-blacklists :? :*
-- :doc getting blacklisted accounts by id
SELECT Tarid FROM Blacklist Where Recid = :id

-- :name get-whitelists :? :*
-- :doc getting whitelisted accounts by id
SELECT Tarid FROM Whitelist Where Recid = :id

-- :name get-requests:? :*
-- :doc getting requsts done by id
SELECT Tarid FROM Request Where Recid = :id

-- :name insert-into-users :insert :raw
-- :doc "Create a new user"
INSERT INTO Users (LastName, FirstName, Birth, Gender) VALUES (:lastname, :firstname, :birth, :gender)

-- :name insert-into-blacklist :insert :raw
-- :doc Create a new blacklist
INSERT INTO Blacklist(Recid, Tarid) VALUES (:recid, :tarid)

-- :name insert-into-whitelist :insert :raw
-- :doc Create a new whitelist
INSERT INTO Whitelist(Recid, Tarid) VALUES (:recid, :tarid)

-- :name insert-into-request :insert :raw
-- :doc Create a new request
INSERT INTO Request(Recid, Tarid) VALUES (:recid, :tarid)
