(defproject matcher-backend "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.postgresql/postgresql "42.2.2"]
                 [com.layerware/hugsql "0.5.1"]]
  :plugins [[cider/cider-nrepl "0.22.4"]]
  :main ^:skip-aot matcher-backend.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
